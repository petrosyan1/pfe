\contentsline {chapter}{\numberline {1}Basic Concepts}{1}{chapter.1}%
\contentsline {section}{The Centimetre and the Second}{1}{section*.3}%
\contentsline {section}{Weight and Mass}{8}{section*.4}%
\contentsline {section}{The SI System and Standards of Measurement}{14}{section*.8}%
\contentsline {section}{Density}{19}{section*.9}%
\contentsline {section}{The Law of Conservation of Mass}{23}{section*.10}%
\contentsline {section}{Action and Reaction}{25}{section*.11}%
\contentsline {section}{How Velocities Are Added}{28}{section*.12}%
\contentsline {section}{Force Is a Vector}{34}{section*.16}%
\contentsline {section}{Inclined Plane}{40}{section*.21}%
\contentsline {chapter}{\numberline {2}Laws of Motion}{45}{chapter.2}%
\contentsline {section}{Various Points of View About Motion}{45}{section*.23}%
\contentsline {section}{The Law of Inertia}{48}{section*.24}%
\contentsline {section}{Motion Is Relative}{53}{section*.25}%
\contentsline {section}{The Point of View of a Celestial Observer}{56}{section*.26}%
\contentsline {section}{Acceleration and Force}{61}{section*.28}%
\contentsline {section}{Rectilinear Motion with Constant Acceleration}{73}{section*.30}%
\contentsline {section}{Path of a Bullet}{78}{section*.31}%
\contentsline {section}{Circular Motion}{83}{section*.34}%
\contentsline {section}{Life at $g$ Zero}{88}{section*.37}%
\contentsline {section}{Motion from an ``Unreasonable'' Point of View}{96}{section*.39}%
\contentsline {section}{Centrifugal Forces}{103}{section*.43}%
\contentsline {section}{Coriolis Forces}{113}{section*.47}%
\contentsline {chapter}{\numberline {3} Conservation Laws}{127}{chapter.3}%
\contentsline {section}{Recoil}{127}{section*.52}%
\contentsline {section}{The Law of Conservation of Momentum}{130}{section*.54}%
\contentsline {section}{Jet Propulsion}{135}{section*.56}%
\contentsline {section}{Motion Under the Action of Gravity}{140}{section*.58}%
\contentsline {section}{The Law of Conservation of Mechanical Energy}{148}{section*.61}%
\contentsline {section}{Work}{153}{section*.62}%
\contentsline {section}{In What Units Work and Energy Are Measured}{157}{section*.64}%
\contentsline {section}{Power and Efficiency of Machines}{159}{section*.65}%
\contentsline {section}{Energy Loss}{162}{section*.66}%
\contentsline {section}{Perpetuum Mobile}{164}{section*.67}%
\contentsline {section}{Collisions}{169}{section*.72}%
\contentsline {chapter}{\numberline {4}Oscillations}{175}{chapter.4}%
\contentsline {section}{Equilibrium}{175}{section*.74}%
\contentsline {section}{Simple Oscillations}{178}{section*.76}%
\contentsline {section}{Displaying Oscillations}{185}{section*.78}%
\contentsline {section}{Force and Potential Energy in Oscillations}{191}{section*.81}%
\contentsline {section}{Spring Vibrations}{196}{section*.83}%
\contentsline {section}{More Complex Oscillations}{201}{section*.85}%
\contentsline {section}{Resonance}{202}{section*.87}%
\contentsline {chapter}{\numberline {5}Motion of Solid Bodies}{207}{chapter.5}%
\contentsline {section}{Torque}{207}{section*.89}%
\contentsline {section}{Lever}{213}{section*.92}%
\contentsline {section}{Loss in Path}{217}{section*.95}%
\contentsline {section}{Other Very Simple Machines}{223}{section*.96}%
\contentsline {section}{Adding Parallel Forces Acting on a Solid Body}{225}{section*.98}%
\contentsline {section}{Centre of Gravity}{231}{section*.102}%
\contentsline {section}{Centre of Mass}{238}{section*.107}%
\contentsline {section}{Angular Momentum}{242}{section*.108}%
\contentsline {section}{Law of Conservation of Angular Momentum}{244}{section*.110}%
\contentsline {section}{Angular Momentum as a Vector}{247}{section*.111}%
\contentsline {section}{Tops}{251}{section*.113}%
\contentsline {section}{Flexible Shaft}{254}{section*.115}%
\contentsline {chapter}{\numberline {6}Gravitation}{261}{chapter.6}%
\contentsline {section}{What Holds the Earth Up!}{261}{section*.116}%
\contentsline {section}{Law of Universal Gravitation}{264}{section*.117}%
\contentsline {section}{Weighing the Earth}{268}{section*.119}%
\contentsline {section}{Measuring $g$ in the Service of Prospecting}{271}{section*.120}%
\contentsline {section}{Weight Underground}{278}{section*.123}%
\contentsline {section}{Gravitational Energy}{282}{section*.125}%
\contentsline {section}{How Planets Move}{289}{section*.126}%
\contentsline {section}{Interplanetary Travel}{298}{section*.131}%
\contentsline {section}{If There Were No Moon}{303}{section*.133}%
\contentsline {chapter}{\numberline {7}Pressure}{315}{chapter.7}%
\contentsline {section}{Hydraulic Press}{315}{section*.137}%
\contentsline {section}{Hydrostatic Pressure}{318}{section*.139}%
\contentsline {section}{Atmospheric Pressure}{323}{section*.142}%
\contentsline {section}{How Atmospheric Pressure Was Discovered}{329}{section*.145}%
\contentsline {section}{Atmospheric Pressure and Weather}{332}{section*.146}%
\contentsline {section}{Change of Pressure with Altitude}{336}{section*.148}%
\contentsline {section}{Archimedes' Principle}{340}{section*.149}%
\contentsline {section}{Extremely Low Pressures. Vacuum}{347}{section*.152}%
\contentsline {section}{Pressures of Millions of Atmospheres}{350}{section*.153}%
